/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { Server } from 'hapi';
const request = require('request');

const init = async () => {
  const server = new Server({
    port: 3333,
    host: 'localhost',
    routes: {
      cors: true
    }
  });

  const getRequestData = function(api, symbol, period, token) {
    const url = `${api}/${symbol}/chart/${period}?token=${token}`;
    return new Promise((resolve, reject) => {
      request(url, (error, response, body) => {
        if (response && response['statusCode'] === 200) {
          resolve(body);
        } else {
          reject(new Error(error));
        }
      });
    });

  };

  server.method('getData', getRequestData, {
    generateKey: (symbol, period) => `${symbol}_${period}`
  });

  server.route({
    method: 'GET',
    path: '/',
    handler: async (request, h) => {
      const symbol = request.query.symbol;
      const period = request.query.period;
      const key = request.query.key;
      const url = request.query.url;

      return server.methods.getData(url, symbol, period, key);
    },
    options: {
      cache: {
        expiresIn: 30 * 1000,
        privacy: 'private'
      }
    }
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
