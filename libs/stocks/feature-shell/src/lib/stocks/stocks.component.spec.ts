import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { provideMockStore } from '@ngrx/store/testing';

import { StocksComponent } from './stocks.component';

describe('StocksComponent', () => {
  let component: StocksComponent;
  let fixture: ComponentFixture<StocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [StocksComponent],
        providers: [
          FormBuilder,
          PriceQueryFacade,
          provideMockStore({initialState: {selectedSymbol: ''}})
        ],
        schemas: [
          NO_ERRORS_SCHEMA
        ]
      })
      .compileComponents();
  }));

  it('should create', () => {
    fixture = TestBed.createComponent(StocksComponent);
    component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });
});
