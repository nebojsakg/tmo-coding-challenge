import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { debounceTime, takeWhile } from 'rxjs/operators';


@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnDestroy, OnInit {

  componentActive: boolean;

  stockPickerForm: FormGroup;

  quotes$ = this.priceQuery.priceQueries$;

  minDate: Date;
  maxDate: Date;

  constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {

  }

  ngOnInit() {
    this.componentActive = true;
    this.createForm();
  }

  ngOnDestroy() {
    this.componentActive = false;
  }

  private createForm(): void {
    this.stockPickerForm = this.fb.group({
      symbol: [null, Validators.required],
      dateFrom: [null, Validators.required],
      dateTo: [null, Validators.required]

    });

    const symbolFormControl = this.stockPickerForm.get('symbol');
    const dateFromFormControl = this.stockPickerForm.get('dateFrom');
    const dateToFormControl = this.stockPickerForm.get('dateTo');

    dateFromFormControl.valueChanges
      .pipe(
        takeWhile(() => this.componentActive)
      )
      .subscribe( (date) => {
        this.minDate = date;
        // if date range is invalid, set it to same dates
        if (!!dateToFormControl.value && dateToFormControl.value < this.minDate) {
          dateToFormControl.patchValue(this.minDate);
        }
      });

    dateToFormControl.valueChanges
      .pipe(
        takeWhile(() => this.componentActive)
      )
      .subscribe( () => this.fetchQuote());


    this.stockPickerForm.valueChanges
      .pipe(
        takeWhile(() => this.componentActive),
        debounceTime(500)
      )
      .subscribe(() => this.fetchQuote());
  }

  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, dateFrom, dateTo } = this.stockPickerForm.value;
      this.priceQuery.fetchQuote(symbol, dateFrom, dateTo);
    }
  }
}
