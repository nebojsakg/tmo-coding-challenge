import { PriceQueryResponse, PriceQuery } from './price-query.type';
import { map, pick } from 'lodash-es';
import { differenceInYears, differenceInMonths, parse } from 'date-fns';

export function transformPriceQueryResponse(
  response: PriceQueryResponse[]
): PriceQuery[] {
  return map(
    response,
    responseItem =>
      ({
        ...pick(responseItem, [
          'date',
          'open',
          'high',
          'low',
          'close',
          'volume',
          'change',
          'changePercent',
          'label',
          'changeOverTime'
        ]),
        dateNumeric: parse(responseItem.date).getTime()
      } as PriceQuery)
  );
}

export function transformDateRangeToPeriod(dateFrom: Date, dateTo: Date): string {

  const diffInYears = differenceInYears(dateTo, dateFrom);
  if (diffInYears >= 5) {
    return 'max';
  } else if (diffInYears >= 3) {
    return '5y';
  } else if (diffInYears >= 1) {
    return '3y';
  } else if (diffInYears > 0) {
    return '1y';
  }

  const diffInMonths = differenceInMonths(dateTo, dateFrom);

  if (diffInMonths >= 6) {
    return '1y';
  } else if (diffInMonths >= 3) {
    return '6m';
  } else if (diffInMonths >= 1) {
    return '3m';
  } else {
    return '1m';
  }
}
