import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import {
  StocksAppConfig,
  StocksAppConfigToken
} from '@coding-challenge/stocks/data-access-app-config';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { transformDateRangeToPeriod } from 'libs/stocks/data-access-price-query/src/lib/+state/price-query-transformer.util';
import { map } from 'rxjs/operators';
import {
  FetchPriceQuery,
  PriceQueryActionTypes,
  PriceQueryFetched,
  PriceQueryFetchError
} from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { PriceQueryResponse } from './price-query.type';

@Injectable()
export class PriceQueryEffects {
  @Effect() loadPriceQuery$ = this.dataPersistence.fetch(
    PriceQueryActionTypes.FetchPriceQuery,
    {
      run: (action: FetchPriceQuery, state: PriceQueryPartialState) => {

        const currentDate = new Date();

        action.dateTo = action.dateTo > currentDate ? currentDate : action.dateTo;

        const period = transformDateRangeToPeriod(action.dateFrom, action.dateTo);

        return this.httpClient
          .get(`/api/`, {params: {symbol: action.symbol, period, key: this.env.apiKey, url: this.env.apiURL}})
          .pipe(
            map((resp: any[]) => resp.filter(item => new Date(item.date) > action.dateFrom && new Date(item.date) < action.dateTo)),
            map(resp => new PriceQueryFetched(resp as PriceQueryResponse[]))
          );
      },

      onError: (action: FetchPriceQuery, error) => {
        return new PriceQueryFetchError(error);
      }
    }
  );

  constructor(
    @Inject(StocksAppConfigToken) private env: StocksAppConfig,
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<PriceQueryPartialState>
  ) {}
}
